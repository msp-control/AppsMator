package com.rzx.appsmator.xposed.mod;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.Log;

import com.rzx.appsmator.util.Config;

import java.util.Locale;

import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

/**
 * Created by Administrator on 2016/11/25.
 */
public class AppLangMod implements IXposedHookLoadPackage {
    private final String TAG = "AppLangMod";

    /**
     * @param loadPackageParam
     * @throws Throwable
     */
    @Override
    public void handleLoadPackage(XC_LoadPackage.LoadPackageParam loadPackageParam) throws Throwable {
        languageHook(loadPackageParam);
    }

    /**
     * @param lpparam
     */
    private void languageHook(final XC_LoadPackage.LoadPackageParam lpparam) {
        XposedHelpers.findAndHookMethod(Resources.class.getName(),
                lpparam.classLoader, "getConfiguration",
                new XC_MethodHook() {
                    @Override
                    protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                        Log.v(TAG, "afterHookedMethod Resources.getConfiguration");
                        Configuration configuration = new Configuration((Configuration) param.getResult());
                        configuration.locale = getLocaleByConf(lpparam.packageName);
                        param.setResult(configuration);
                    }
                });
    }

    /**
     * @param pkgName
     * @return
     */
    private Locale getLocaleByConf(String pkgName) {
        String lang = Config.getInstance().getProperty(pkgName + ".LANG", "zh_CN ");
        if (lang.equals("zh_CN")) {
            return Locale.SIMPLIFIED_CHINESE;
        } else if (lang.equals("en_US")) {
            return Locale.US;
        }

        return Locale.SIMPLIFIED_CHINESE;
    }
}
