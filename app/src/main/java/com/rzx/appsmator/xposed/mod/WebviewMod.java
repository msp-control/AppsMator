package com.rzx.appsmator.xposed.mod;

import android.webkit.WebView;

import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

/**
 * Created by Administrator on 2017/3/3.
 */
public class WebviewMod implements IXposedHookLoadPackage {

    public void handleLoadPackage(XC_LoadPackage.LoadPackageParam lpparam) throws Throwable {
//        if (! lpparam.packageName.equals("com.tencent.mm")) {
//            return;
//        }

        XposedBridge.log("WebViewHook handleLoadPackage: " + lpparam.packageName);
        // 勾住 WebView 所有的构造器
        XposedBridge.hookAllConstructors(WebView.class, new XC_MethodHook() {
            @Override
            protected void beforeHookedMethod(XC_MethodHook.MethodHookParam param) throws Throwable {
                // 打开webContentsDebuggingEnabled
                XposedHelpers.callStaticMethod(WebView.class, "setWebContentsDebuggingEnabled", true);
            }
        });

        // 避免手动设置为false情况
        XposedHelpers.findAndHookMethod(WebView.class.getName()
            , lpparam.classLoader
            , "setWebContentsDebuggingEnabled"
            , boolean.class
            , new XC_MethodHook() {
                @Override
                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                    param.args[0] = true;
                }
            });


    }
}
