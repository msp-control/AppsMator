package com.rzx.appsmator.xposed.mod;

import android.bluetooth.BluetoothAdapter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.res.Configuration;
import android.net.wifi.ScanResult;
import android.telephony.NeighboringCellInfo;
import android.telephony.PhoneStateListener;
import android.text.TextUtils;
import android.util.Log;

import com.rzx.appsmator.xposed.ParamPro;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage.LoadPackageParam;

public class DevUniqueMod implements IXposedHookLoadPackage {
    private final String TAG = "DevUniqueMod";

    @Override
	public void handleLoadPackage(LoadPackageParam lpparam) throws Throwable {
		// TODO Auto-generated method stub
		if (!lpparam.packageName.equals("com.tencent.mm")
				&& !lpparam.packageName.equals("com.rzx.appsmator")
				&& !lpparam.packageName
						.equals("com.samuelzhan.readphoneparameter")
				&& !lpparam.packageName.equals("com.rzx.systemparam")
                && !lpparam.packageName.equals("com.facebook.katana")
                && !lpparam.packageName.equals("com.vpn.tianxing"))
			return;
		Log.e("zzzzz", "newhook");
		// http代理
		httpAgentHook(lpparam);
		// 设备硬件信息
		buildHook(lpparam);
		// imei imsi 基站
		teleHook(lpparam);
		// wifi mac
		wifiHook(lpparam);
		// 蓝牙
		bluetoolHook(lpparam);

		appListHook(lpparam);

        // app语言
//        languageHook(lpparam);

	}

//    /**
//     * @param lpparam
//     */
//    private void languageHook(final LoadPackageParam lpparam) {
//        XposedHelpers.findAndHookMethod(Resources.class.getName(),
//                lpparam.classLoader, "getConfiguration",
//                new XC_MethodHook() {
//                    @Override
//                    protected void afterHookedMethod(MethodHookParam param) throws Throwable {
//                        Log.v(TAG, "afterHookedMethod Resources.getConfiguration");
//                        Configuration configuration = new Configuration((Configuration) param.getResult());
//                        configuration.locale = getLocaleByConf(lpparam.packageName);
//                        param.setResult(configuration);
//                    }
//                });
//    }

    // 手机系统信息build
	private void buildHook(final LoadPackageParam lpparam) {

		stringFieldHook(android.os.Build.class, "SERIAL", "ro.build.serial");
		stringFieldHook(android.os.Build.class, "ID", "ro.build.id");
		stringFieldHook(android.os.Build.class, "DISPLAY",
				"ro.build.display.id");
		stringFieldHook(android.os.Build.VERSION.class, "INCREMENTAL",
				"ro.build.version.incremental");
		stringFieldHook(android.os.Build.VERSION.class, "CODENAME",
				"ro.build.version.codename");
		stringFieldHook(android.os.Build.VERSION.class, "RELEASE",
				"ro.build.version.release");
		longFieldHook(android.os.Build.class, "TIME", "ro.build.date.utc");
		stringFieldHook(android.os.Build.class, "TYPE", "ro.build.type");
		stringFieldHook(android.os.Build.class, "USER", "ro.build.user");
		stringFieldHook(android.os.Build.class, "HOST", "ro.build.host");
		stringFieldHook(android.os.Build.class, "TAGS", "ro.build.tags");
		stringFieldHook(android.os.Build.class, "MODEL", "ro.product.model");
		stringFieldHook(android.os.Build.class, "BRAND", "ro.product.brand");
		stringFieldHook(android.os.Build.class, "PRODUCT", "ro.product.name");
		stringFieldHook(android.os.Build.class, "DEVICE", "ro.product.device");
		stringFieldHook(android.os.Build.class, "BOARD", "ro.product.board");
		stringFieldHook(android.os.Build.class, "MANUFACTURER",
				"ro.product.manufacturer");
		stringFieldHook(android.os.Build.class, "FINGERPRINT",
				"ro.build.fingerprint");
		stringFieldHook(android.os.Build.class, "HARDWARE", "ro.build.hardware");

		XposedHelpers.findAndHookMethod(android.os.Build.class.getName(),
				lpparam.classLoader, "getRadioVersion", new XC_MethodHook() {
					@Override
					protected void afterHookedMethod(MethodHookParam param)
							throws Throwable {
						// TODO Auto-generated method stub
						super.afterHookedMethod(param);
						param.setResult(getValue("ro.build.radioversion"));

					}
				});

		XposedHelpers.findAndHookMethod("android.os.SystemProperties",
				lpparam.classLoader, "get", String.class, new XC_MethodHook() {
					// 为了防止某些APP跳过Build�?
					// 而直接使用SystemProperties.native_get(String,
					// String)或�?SystemProperies.get(String)获得参数

					@Override
					protected void afterHookedMethod(MethodHookParam param)
							throws Throwable {
						try {
							String key = (String) param.args[0];
							String value = getValue(key);
							String result = (String) param.getResult();
							if (!TextUtils.isEmpty(result) && value != null) {

								if (value.equals("null"))
									param.setResult(null);
								param.setResult(value);
							}
						} catch (Exception e) {
							StringWriter sw = new StringWriter();
							e.printStackTrace(new PrintWriter(sw, true));
							String str = sw.toString();

						}
					}
				});

		XposedHelpers.findAndHookMethod("android.os.SystemProperties",
				lpparam.classLoader, "get", String.class, String.class,
				new XC_MethodHook() {
					// 为了防止某些APP跳过Build�?
					// 而直接使用SystemProperties.native_get(String,
					// String)或�?SystemProperies.get(String)获得参数

					@Override
					protected void afterHookedMethod(MethodHookParam param)
							throws Throwable {
						super.afterHookedMethod(param);
						try {
							String key = (String) param.args[0];
							String value = getValue(key);
							String result = (String) param.getResult();
							if (!TextUtils.isEmpty(result) && value != null) {

								if (value.equals("null"))
									param.setResult(null);
								param.setResult(value);
							}

						} catch (Exception e) {
							StringWriter sw = new StringWriter();
							e.printStackTrace(new PrintWriter(sw, true));
							String str = sw.toString();

						}
					}
				});

	}

	private void appListHook(LoadPackageParam lpparam) {
		// TODO Auto-generated method stub
		XposedHelpers.findAndHookMethod(
				"android.app.ApplicationPackageManager", lpparam.classLoader,
				"getInstalledPackages", int.class, new XC_MethodHook() {
					@Override
					protected void afterHookedMethod(MethodHookParam param)
							throws Throwable {
						List<PackageInfo> packageInfos = (List<PackageInfo>) param
								.getResult();
						Iterator iterator = packageInfos.iterator();
						while (iterator.hasNext()) {
							if (((PackageInfo) iterator.next()).packageName
									.equals("de.robv.android.xposed.installer"))
								iterator.remove();
						}
						param.setResult(packageInfos);
					}
				});

		XposedHelpers.findAndHookMethod(
				"android.app.ApplicationPackageManager", lpparam.classLoader,
				"getInstalledApplications", int.class, new XC_MethodHook() {
					@Override
					protected void afterHookedMethod(MethodHookParam param)
							throws Throwable {
						List<ApplicationInfo> applicationInfos = (List<ApplicationInfo>) param
								.getResult();
						Iterator iterator = applicationInfos.iterator();
						while (iterator.hasNext()) {
							if (((ApplicationInfo) iterator.next()).packageName
									.equals("de.robv.android.xposed.installer"))
								iterator.remove();
						}
						param.setResult(applicationInfos);
					}
				});

	}

	private void httpAgentHook(LoadPackageParam lpparam) {
		XposedHelpers.findAndHookMethod(System.class.getName(),
				lpparam.classLoader, "getProperty", String.class,
				new XC_MethodHook() {
					@Override
					protected void afterHookedMethod(MethodHookParam param)
							throws Throwable {
						super.afterHookedMethod(param);
						if (param.args[0].toString().equals("http.agent")) {
							StringBuffer sb = new StringBuffer();
							sb.append("Dalvik/1.6.0 (Linux; U; Android ");
							sb.append(getValue("ro.build.version.release"));
							sb.append("; ");
							sb.append(getValue("ro.product.model"));
							sb.append(" Build/");
							sb.append(getValue("ro.build.id"));
							param.setResult(sb.toString());

						}

					}
				});
	}

	private void teleHook(LoadPackageParam lpparam) {
		XposedHelpers.findAndHookMethod(
				android.telephony.TelephonyManager.class.getName(),
				lpparam.classLoader, "getDeviceId", StringMethodHook("imei"));
		XposedHelpers.findAndHookMethod(
				"com.android.internal.telephony.PhoneSubInfo",
				lpparam.classLoader, "getDeviceId", StringMethodHook("imei"));

		XposedHelpers.findAndHookMethod(
				android.telephony.TelephonyManager.class.getName(),
				lpparam.classLoader, "getSubscriberId",
				StringMethodHook("imsi"));

		XposedHelpers.findAndHookMethod(
				android.telephony.TelephonyManager.class.getName(),
				lpparam.classLoader, "getSimSerialNumber",
				StringMethodHook("simserial"));
		XposedHelpers.findAndHookMethod(
				android.telephony.TelephonyManager.class.getName(),
				lpparam.classLoader, "getSimOperator", new XC_MethodHook() {
					@Override
					protected void afterHookedMethod(MethodHookParam param)
							throws Throwable {
						// TODO Auto-generated method stub
						super.afterHookedMethod(param);
						Object result = param.getResult();
						if (result == null)
							return;
						if (result.toString().equals(""))
							return;

						String value = getValue("imsi");
						if (value.equals("null"))
							value = null;
						if (!TextUtils.isEmpty(value)) {
							param.setResult(value.substring(0, 5));
						}
					}
				});
		XposedHelpers.findAndHookMethod(
				android.content.res.Resources.class.getName(),
				lpparam.classLoader, "getConfiguration", new XC_MethodHook() {
					// 此处的mnc和mcc必须和系统中其他关于运营商的数据对应!
					protected void afterHookedMethod(MethodHookParam param)
							throws Throwable {
						Configuration cfg = (Configuration) param.getResult();

						if (cfg.mnc != 0)
							cfg.mnc = Integer.parseInt(getValue("mnc"));
						if (cfg.mcc != 0)
							cfg.mcc = Integer.parseInt(getValue("mcc"));

					}

				});

		// getNeighboringCellInfo 设置四周cell为空
		XposedHelpers.findAndHookMethod(
				android.telephony.TelephonyManager.class.getName(),
				lpparam.classLoader, "getNeighboringCellInfo",
				new XC_MethodHook() {
					@Override
					protected void afterHookedMethod(MethodHookParam param)
							throws Throwable {
						// TODO Auto-generated method stub

						param.setResult(new ArrayList<NeighboringCellInfo>());

					}

				});

		// 基站 lac cid
		XposedHelpers.findAndHookMethod(
				android.telephony.gsm.GsmCellLocation.class.getName(),
				lpparam.classLoader, "getLac", new XC_MethodHook() {
					@Override
					protected void afterHookedMethod(MethodHookParam param)
							throws Throwable {
						param.setResult(Integer.valueOf(getValue("cell.lac")));
					}
				});
		XposedHelpers.findAndHookMethod(
				android.telephony.gsm.GsmCellLocation.class.getName(),
				lpparam.classLoader, "getCid", new XC_MethodHook() {
					@Override
					protected void afterHookedMethod(MethodHookParam param)
							throws Throwable {
						param.setResult(Integer.valueOf(getValue("cell.cid")));
					}
				});

		XposedHelpers.findAndHookMethod("android.telephony.CellIdentityGsm",
				lpparam.classLoader, "getLac", new XC_MethodHook() {
					@Override
					protected void afterHookedMethod(MethodHookParam param)
							throws Throwable {
						param.setResult(Integer.valueOf(getValue("cell.lac")));
					}
				});
		XposedHelpers.findAndHookMethod("android.telephony.CellIdentityGsm",
				lpparam.classLoader, "getCid", new XC_MethodHook() {
					@Override
					protected void afterHookedMethod(MethodHookParam param)
							throws Throwable {
						param.setResult(Integer.valueOf(getValue("cell.cid")));
					}
				});

		// android/telephony/CellIdentityLte;->getTac()I
		XposedHelpers.findAndHookMethod("android.telephony.CellIdentityLte",
				lpparam.classLoader, "getTac", new XC_MethodHook() {
					@Override
					protected void afterHookedMethod(MethodHookParam param)
							throws Throwable {
						param.setResult(Integer.valueOf(getValue("cell.lac")));
					}
				});
		XposedHelpers.findAndHookMethod("android.telephony.CellIdentityLte",
				lpparam.classLoader, "getCi", new XC_MethodHook() {
					@Override
					protected void afterHookedMethod(MethodHookParam param)
							throws Throwable {
						param.setResult(Integer.valueOf(getValue("cell.cid")));
					}
				});

		XposedHelpers.findAndHookMethod("android.telephony.TelephonyManager",
				lpparam.classLoader, "listen", PhoneStateListener.class,
				int.class, new XC_MethodHook() {
					protected void beforeHookedMethod(MethodHookParam param)
							throws Throwable {
						int value = (Integer) param.args[1];
						if ((value & 0x10) != 0) {
							param.args[1] = value - 0x10;
						}

					}

				});
	}

	private void wifiHook(LoadPackageParam lpparam) {
		XposedHelpers.findAndHookMethod(
				android.net.wifi.WifiInfo.class.getName(), lpparam.classLoader,
				"getMacAddress", StringMethodHook("mac"));
		XposedHelpers.findAndHookMethod(
				android.net.wifi.WifiInfo.class.getName(), lpparam.classLoader,
				"getBSSID", StringMethodHook("bssid"));
		XposedHelpers.findAndHookMethod(
				android.net.wifi.WifiInfo.class.getName(), lpparam.classLoader,
				"getSSID", StringMethodHook("ssid"));
		XposedHelpers.findAndHookMethod(
				android.net.wifi.WifiInfo.class.getName(), lpparam.classLoader,
				"getIpAddress", intMethodHook("ipAddress"));

		XposedHelpers.findAndHookMethod(
				android.net.wifi.WifiManager.class.getName(),
				lpparam.classLoader, "getScanResults", new XC_MethodHook() {
					@Override
					protected void afterHookedMethod(MethodHookParam param)
							throws Throwable {
						super.afterHookedMethod(param);
						List<ScanResult> scanResults = (List<ScanResult>) param
								.getResult();
						Iterator iterator = scanResults.iterator();
						ScanResult sr = null;
						if (iterator.hasNext()) {
							sr = (ScanResult) iterator.next();
						}
						if (sr != null) {
							List<ScanResult> list = new ArrayList<ScanResult>();
							sr.SSID = getValue("ssid");
							sr.BSSID = getValue("bssid");
							list.add(sr);
							param.setResult(list);
						}

					}
				});

	}

	// 蓝牙
	private void bluetoolHook(LoadPackageParam lpparam) {
		XposedHelpers.findAndHookMethod(BluetoothAdapter.class.getName(),
				lpparam.classLoader, "getAddress", StringMethodHook("bluemac"));
		XposedHelpers.findAndHookMethod(BluetoothAdapter.class.getName(),
				lpparam.classLoader, "getName", StringMethodHook("bluename"));

	}

	private Object StringMethodHook(final String key) {
		// TODO Auto-generated method stub
		XC_MethodHook methodHook = new XC_MethodHook() {
			@Override
			protected void afterHookedMethod(MethodHookParam param)
					throws Throwable {
				// TODO Auto-generated method stub
				super.afterHookedMethod(param);
				String value = getValue(key);
				Object result = param.getResult();
				if (result == null)
					return;
				if (!result.toString().equals("")
						&& !result.toString().contains("unknown")
						&& value != null) {
					if (value.equals("null"))
						value = null;
					param.setResult(value);
				}
			}
		};

		return methodHook;
	}

	private Object intMethodHook(final String key) {
		// TODO Auto-generated method stub
		XC_MethodHook methodHook = new XC_MethodHook() {
			@Override
			protected void afterHookedMethod(MethodHookParam param)
					throws Throwable {
				// TODO Auto-generated method stub
				super.afterHookedMethod(param);
				int result = (int) param.getResult();
				if (result != 0 && getValue(key) != null)
					param.setResult(Integer.parseInt(getValue(key)));
			}

		};

		return methodHook;
	}

	private void stringFieldHook(final Class<?> className, String fieldName,
			String key) {
		if (getValue(key) != null)

			XposedHelpers.setStaticObjectField(className, fieldName,
					getValue(key));
	}

	private void longFieldHook(final Class<?> className, String fieldName,
			String key) {
		if (getValue(key) != null)

			XposedHelpers.setStaticObjectField(className, fieldName, Long
					.valueOf(getValue(key)).longValue());
	}

	private String getValue(String key) {
		// TODO Auto-generated method stub
		return ParamPro.getValue(key);
	}

//    private Locale getLocaleByConf(String pkgName) {
//        String lang = Config.getInstance().getProperty(pkgName + ".LANG", "zh_CN ");
//        if (lang.equals("zh_CN")) {
//            return Locale.SIMPLIFIED_CHINESE;
//        } else if (lang.equals("en_US")) {
//            return Locale.US;
//        }
//
//        return Locale.SIMPLIFIED_CHINESE;
//    }

}
