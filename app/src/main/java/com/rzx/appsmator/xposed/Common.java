package com.rzx.appsmator.xposed;

public class Common {

    public static final String TAG = "AppsMator";
    public static final String MY_PACKAGE_NAME = Common.class.getPackage().getName();

    public static final String PREFS = "appsmator_settings";

    public static final String DEFAULT_LOCALE = "Default";

}
