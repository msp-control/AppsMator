package com.rzx.appsmator.xposed;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

import android.annotation.SuppressLint;

public class ParamPro {
	static String propPath = "mnt/sdcard/GodHand/res/param.properties";

	@SuppressLint("NewApi")
	public static String getValue(String key) {

		try {
			Properties pps = new Properties();
			FileInputStream inputStream = new FileInputStream(propPath);
			BufferedReader bf = new BufferedReader(new InputStreamReader(
					inputStream, "utf-8"));
			pps.load(bf);
			inputStream.close();
			return pps.getProperty(key);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
