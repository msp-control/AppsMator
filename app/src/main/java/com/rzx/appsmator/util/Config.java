package com.rzx.appsmator.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

/**
 * Created by Administrator on 2016/10/14.
 */
public class Config extends Properties{
    private static Config mConfig = null;
    private static final String mDefaultFile = "/mnt/sdcard/GodHand/res/conf.pro";

    /**
     * @return
     */
    public static Config getInstance(){
        if (mConfig == null){
            mConfig = new Config();
            File file = new File(mDefaultFile);
            if (!file.exists()){
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            try {
                InputStream in = new FileInputStream(file);
                mConfig.load(in);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return mConfig;
    }

    public void save(){
        if(mConfig != null){
            try {
                OutputStream out = new FileOutputStream(mDefaultFile);
                mConfig.store(out, "configuration");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
