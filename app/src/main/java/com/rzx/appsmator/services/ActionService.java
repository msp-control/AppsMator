package com.rzx.appsmator.services;

import android.app.Service;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import com.rzx.appsmator.XToast;
import com.rzx.appsmator.model.Tb_contacts;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/8/6.
 */
public class ActionService extends Service {

    private static final int MSG_TOAST = 0x1000001;
    private final Handler msgHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.arg1) {
                case MSG_TOAST:
                    XToast.makeText(getApplicationContext(), msg.getData().getString("text"), XToast.LENGTH_SHORT).show();
                    break;
                default:
                    break;
            }
        }
    };

    private final IAidlActionService.Stub mBinder = new IAidlActionService.Stub() {

        @Override
        public void toast(String str) throws RemoteException {
            _toast(str);
        }

        @Override
        public String getDeviceId() throws RemoteException {
            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            return telephonyManager.getDeviceId();
        }

        @Override
        public void addContacts(List<Tb_contacts> contacts) throws RemoteException {
            try {
                _addContactBatch(contacts);
            } catch (OperationApplicationException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void clearContacts() throws RemoteException {
            _clearContacts();
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
//        if (intent == null){
//            stopSelf();
//            return super.onStartCommand(intent, flags, startId);
//        }
//
//        Bundle bundle = intent.getExtras();
//        String cmd = bundle.getString("cmd", "");
//
//        if (cmd.equals("toast")){
//            makeToast();
//        } else if(cmd.equals("getDeviceId")){
//            getDeviceId();
//        } else if(cmd.equals("addContacts")){
////            String file = bundle.getString("file", "");
////            String fileData = FileUtils.getFilString(file);
////
////            String[] lines = fileData.split("\n");
////            for (int i = 0; i < lines.length; ++i){
////                String[] line = lines[i].split(",");
////                if (line.length == 2){
////                    insertContact(line[0], line[1]);
////                }
////            }
//
//            List<Tb_contacts> tb_contactses = new ArrayList<>();
//            tb_contactses.add(new Tb_contacts("a", "12580"));
//            tb_contactses.add(new Tb_contacts("b", "12580"));
//            tb_contactses.add(new Tb_contacts("c", "12580"));
//            tb_contactses.add(new Tb_contacts("d", "12580"));
//            tb_contactses.add(new Tb_contacts("e", "12580"));
//            tb_contactses.add(new Tb_contacts("f", "12580"));
//            try {
//                addContactBatch(tb_contactses);
//            } catch (RemoteException e) {
//                e.printStackTrace();
//            } catch (OperationApplicationException e) {
//                e.printStackTrace();
//            }
//        } else if(cmd.equals("clearContacts")){
//            clearContacts();
//        }
//        stopSelf();
        return super.onStartCommand(intent, flags, startId);
    }

//    /**
//     *
//     */
//    private void makeToast(){
//        String text = "";
//        try {
//            FileInputStream fin = new FileInputStream("/mnt/sdcard/GodHand/tmp/ActionService_makeToast.txt");
//            int length = fin.available();
//            byte[] buffer = new byte[length];
//            fin.read(buffer);
//            text = EncodingUtils.getString(buffer, "UTF-8");
//            Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
//            fin.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    /**
//     *
//     */
//    private void getDeviceId(){
//        TelephonyManager telephonyManager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
//        String imei = telephonyManager.getDeviceId();
//
//        File destDir = new File("/mnt/sdcard/GodHand/tmp");
//        if (!destDir.exists()) {
//            destDir.mkdirs();
//        }
//        FileUtils.writeFile("/mnt/sdcard/GodHand/tmp/ActionService_getDeviceId.txt", imei);
//    }
//
//    /**
//     * 添加通讯录联系人
//     *
//     * @param name 姓名
//     * @param phone 电话
//     */
//    public void insertContact(String name, String phone) {
//		/* 往 raw_contacts 中添加数据，并获取添加的id号 */
//        Uri uri = Uri.parse("content://com.android.contacts/raw_contacts");
//        ContentResolver resolver = getContentResolver();
//        ContentValues values = new ContentValues();
//        long contactId = ContentUris.parseId(resolver.insert(uri, values));
//
//		/* 往 data 中添加数据（要根据前面获取的id号） */
//        // 添加姓名
//        uri = Uri.parse("content://com.android.contacts/data");
//        values.put("raw_contact_id", contactId);
//        values.put("mimetype", "vnd.android.cursor.item/name");
//        values.put(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, name);
//        resolver.insert(uri, values);
//
//        // 添加电话
//        values.clear();
//        values.put("raw_contact_id", contactId);
//        values.put("mimetype", "vnd.android.cursor.item/phone_v2");
//        values.put("data2", "2");
//        values.put("data1", phone);
//        resolver.insert(uri, values);
//    }

    public void _toast(String text) {
        Message msg = msgHandler.obtainMessage();
        msg.arg1 = MSG_TOAST;
        Bundle bundle = new Bundle();
        bundle.putString("text", text);
        msg.setData(bundle);
        msgHandler.sendMessage(msg);
    }

    /**
     * 清空通讯录
     *
     */
    public void _clearContacts() {
        ContentResolver cr = getContentResolver();
        cr.delete(ContactsContract.RawContacts.CONTENT_URI.buildUpon()
                .appendQueryParameter(ContactsContract.CALLER_IS_SYNCADAPTER,"true")
                .build(), "_id!=-1", null);
//        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null,
//                null, null, null);
//        while (cur.moveToNext()) {
//            String lookupKey = cur.getString(cur
//                    .getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY));
//            Uri uri = Uri
//                    .withAppendedPath(
//                            ContactsContract.Contacts.CONTENT_LOOKUP_URI,
//                            lookupKey);
//            cr.delete(uri, null, null);
//        }
    }

    /**
     * 批量添加通讯录
     *
     * @throws OperationApplicationException
     * @throws RemoteException
     */
    public void _addContactBatch(List<Tb_contacts> list)
            throws RemoteException, OperationApplicationException {
        ArrayList<ContentProviderOperation> ops = new ArrayList<>();
        int rawContactInsertIndex;
        for (Tb_contacts contact : list) {
            rawContactInsertIndex = ops.size();

            ops.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
                    .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                    .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
                    .withYieldAllowed(true).build());

            // 添加姓名
            ops.add(ContentProviderOperation
                    .newInsert(
                            android.provider.ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Contacts.Data.RAW_CONTACT_ID,
                            rawContactInsertIndex)
                    .withValue(ContactsContract.Contacts.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, contact.getName())
                    .withYieldAllowed(true).build());
            // 添加号码
            ops.add(ContentProviderOperation
                    .newInsert(
                            android.provider.ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Contacts.Data.RAW_CONTACT_ID,
                            rawContactInsertIndex)
                    .withValue(ContactsContract.Contacts.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, contact.getNumber())
                    .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
                    .withValue(ContactsContract.CommonDataKinds.Phone.LABEL, "").withYieldAllowed(true).build());
        }
        if (ops != null) {
            // 批量添加
            getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
        }
    }


}
