package com.rzx.appsmator.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Administrator on 2016/11/16.
 */
public class Tb_contacts implements Parcelable {
    private String name;
    private String number;

    public Tb_contacts() {
        super();
    }

    public Tb_contacts(String name, String number) {
        super();
        this.name = name;
        this.number = number;
    }

    protected Tb_contacts(Parcel in) {
        name = in.readString();
        number = in.readString();
    }

    public static final Creator<Tb_contacts> CREATOR = new Creator<Tb_contacts>() {
        @Override
        public Tb_contacts createFromParcel(Parcel in) {
            return new Tb_contacts(in);
        }

        @Override
        public Tb_contacts[] newArray(int size) {
            return new Tb_contacts[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "Tb_contacts [name=" + name + ", number=" + number + "]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(number);
    }
}
