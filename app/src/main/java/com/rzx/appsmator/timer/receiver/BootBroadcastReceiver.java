package com.rzx.appsmator.timer.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;

import com.rzx.appsmator.cmd.Cmd;
import com.rzx.appsmator.daemon.DaemonRunner;
import com.rzx.appsmator.timer.service.UploadPOIService;
import com.rzx.appsmator.timer.utils.Constants;
import com.rzx.appsmator.timer.utils.ServiceUtil;

/**
 * Created by coder80 on 2014/11/3.
 */
public class BootBroadcastReceiver extends BroadcastReceiver {
	private Context mContext;

	@Override
	public void onReceive(final Context context, Intent intent) {
		mContext = context;
		if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)
				|| intent.getAction().equals(Intent.ACTION_USER_PRESENT)) {

            Cmd.execRootCmd("setprop service.adb.tcp.port 5555");
            Cmd.execRootCmd("stop adbd");
            Cmd.execRootCmd("start adbd");

            // 开机是否自动启动
            Boolean bAutoRun = PreferenceManager
                    .getDefaultSharedPreferences(mContext)
                    .getBoolean("auto_run_switch", false);
            if (!bAutoRun){
                return;
            }

            // 脚本循环是否开启
            bAutoRun = PreferenceManager
                    .getDefaultSharedPreferences(mContext)
                    .getBoolean("loop_run_switch", false);
            if (bAutoRun){
                DaemonRunner.startLoopRun();
            }

            // 心跳服务是否开启
            bAutoRun = PreferenceManager
                    .getDefaultSharedPreferences(mContext)
                    .getBoolean("heartbeat_switch", false);
            if (bAutoRun) {
                Handler handler = new Handler(Looper.getMainLooper());
                // after reboot the device,about 2 minutes later,upload the POI info
                // to server
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (!ServiceUtil.isServiceRunning(mContext,
                                Constants.POI_SERVICE)) {
                            ServiceUtil.invokeTimerService(mContext, UploadPOIService.class, Constants.POI_SERVICE_ACTION);
                        }
                    }
                }, Constants.BROADCAST_ELAPSED_TIME_DELAY);
            }
		}
	}
}
