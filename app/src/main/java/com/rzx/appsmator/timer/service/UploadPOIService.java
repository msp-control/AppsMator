package com.rzx.appsmator.timer.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;

import com.rzx.appsmator.cmd.Cmd;
import com.rzx.appsmator.heartdata.HeartData;
import com.rzx.appsmator.util.HttpUtils;
import com.rzx.appsmator.util.PreferenceMgr;

import org.apache.http.client.ClientProtocolException;

import java.io.IOException;

/**
 * Created by coder80 on 2014/10/31.
 */
public class UploadPOIService extends Service implements Runnable {

	@Override
	public void onCreate() {
		super.onCreate();
		uploadPOIInfo();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

	}

	private void uploadPOIInfo() {

		new Thread(this).start();

	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
        Boolean bRun = PreferenceManager
                .getDefaultSharedPreferences(this)
                .getBoolean("heartbeat_switch", false);
        if (bRun) {
            try {
                String serverIp = PreferenceMgr.getSharedValue(this, "server_ip",
                        "192.168.70.97");
                String serverPort = PreferenceMgr.getSharedValue(this, "server_port", "3002");

                String url = serverIp  + ":" + serverPort + "/mobile_info";
                String postData = HeartData.getInstance().getData(this);

                Boolean bSSL = PreferenceManager
                        .getDefaultSharedPreferences(this)
                        .getBoolean("ssl_switch", false);
                if (bSSL) {
                    url = "https://" + url;
                    Log.e("zmark_httppost", "Start_HttpPost:" + url + " json:" + postData);
                    String strRes = Cmd.execRootCmd("curl -w%{http_code} -k -s -d '" + postData + "' "  + url);
                    Log.e("zmark_httppost", " response:" + strRes);
                } else {
                    url = "http://" + url;
                    HttpUtils.getResultForHttpPost(url, postData);
                }
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        stopSelf();
	}
}
