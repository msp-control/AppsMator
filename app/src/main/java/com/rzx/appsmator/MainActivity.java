package com.rzx.appsmator;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.rzx.appsmator.cmd.Cmd;
import com.rzx.appsmator.timer.service.UploadPOIService;
import com.rzx.appsmator.timer.utils.Constants;
import com.rzx.appsmator.timer.utils.ServiceUtil;
import com.rzx.appsmator.util.Config;
import com.rzx.appsmator.util.FileUtils;
import com.rzx.appsmator.util.HttpUtils;
import com.rzx.appsmator.util.ThreadPool;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class MainActivity extends AppCompatActivity {

    /**
     * The {@link PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    private String mCurrentRunFile;
    private static final String START_UIAUTOMATOR =
            "am instrument --user 0 -w -r -e disableAnalytics true -e debug false -e class com.rzx.godhandmator.AutomatorTest com.rzx.godhandmator.test/android.support.test.runner.AndroidJUnitRunner";
    private static final String STOP_UIAUTOMATOR1 =
            "am force-stop com.rzx.godhandmator";
    private static final String STOP_UIAUTOMATOR2 =
            "am force-stop com.rzx.godhandmator.test";

    private static final int MSG_TOAST = 0x1000001;
    private AudioManager mAudioManager;
    private VolumeReceiver mVolumeReceiver;
    private long mTimeout = 0;
    private int mOldVolume = -1;
    private volatile boolean mIsStarted = false;
    private volatile boolean mIsAlarmStarted = false;

    private final Handler msgHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.arg1) {
                case MSG_TOAST:
                    XToast.makeText(getApplicationContext(), msg.getData().getString("text"), XToast.LENGTH_SHORT).show();
                    break;
                default:
                    break;
            }
        }
    };
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        if (!ServiceUtil.isServiceRunning(this, Constants.POI_SERVICE)) {
            ServiceUtil.invokeTimerService(this, UploadPOIService.class, Constants.POI_SERVICE_ACTION);
        }

        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        registerVolumeChangeReceiver();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.rzx.appsmator/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.rzx.appsmator/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final int REFRESH_COMPLETE = 0X110;
        private static final int ITEM_CLICK_EVENT = 0X111;
        private SwipeRefreshLayout mSwipeLayout;
        private ArrayAdapter<String> mAdapter;
        private List<String> mDatas = new ArrayList<>();
        private ListView listView;

        private Handler mHandler = new Handler() {
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case REFRESH_COMPLETE:
                        refreshFiles();
                        mAdapter.notifyDataSetChanged();
                        mSwipeLayout.setRefreshing(false);
                        break;

                    case ITEM_CLICK_EVENT:
                        Toast.makeText(getContext(), mDatas.get(msg.arg1), Toast.LENGTH_SHORT).show();
                        File file = new File("/mnt/sdcard/GodHand/tmp");
                        if (!file.exists()) {
                            file.mkdirs();
                        }
//                        FileUtils.writeFile("/mnt/sdcard/GodHand/tmp/run_file", mDatas.get(msg.arg1));
                        Config.getInstance().setProperty("run_file", mDatas.get(msg.arg1));
                        Config.getInstance().save();
                        break;
                }
            }
        };

        public PlaceholderFragment() {
        }

        private void refreshFiles() {
            mDatas.clear();
            File specItemDir = new File("/mnt/sdcard/GodHand/lua");
            if (!specItemDir.exists()) {
                specItemDir.mkdir();
            }
            final File[] files = specItemDir.listFiles();
            for (File spec : files) {
                mDatas.add(spec.getName());
            }
            initSelectItem();
        }

        /**
         * @param str
         */
        private int findPositionByString(String str) {
            int default_i = -1;
            for (int i = 0; i < mDatas.size(); ++i) {
                if (mDatas.get(i).equals(str)) {
                    return i;
                }

                if (mDatas.get(i).equals("main.lua")) {
                    default_i = i;
                }
            }
            return default_i;
        }

        /**
         *
         */
        private void initSelectItem() {
//            String runFile = FileUtils.getFilString("/sdcard/GodHand/tmp/run_file");
            String runFile = Config.getInstance().getProperty("run_file");
            if (runFile == null){
                return;
            }
//            if (runFile != null) {
//                runFile = runFile.replaceAll("\r|\n", "");
//            }
            int pos = findPositionByString(runFile);
            if (pos != -1) {
                listView.setItemChecked(pos, true);
            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);

            listView = (ListView) rootView.findViewById(R.id.section_label);
            listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

            refreshFiles();

            mSwipeLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.id_swipe_ly);
            mSwipeLayout.setOnRefreshListener(this);
            mSwipeLayout.setColorSchemeColors(android.R.color.holo_blue_bright, android.R.color.holo_green_light,
                    android.R.color.holo_orange_light, android.R.color.holo_red_light);

            mAdapter = new ArrayAdapter<String>(getContext(), R.layout.list_item, mDatas) {
                @Override
                public View getView(int position, View convertView, ViewGroup parent) {
                    ChoiceView view;
                    if (convertView == null) {
                        view = new ChoiceView(getContext());
                    } else {
                        view = (ChoiceView) convertView;
                    }
                    view.setText(getItem(position));

                    return view;
                }
            };
            listView.setAdapter(mAdapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Message msg = new Message();
                    msg.what = ITEM_CLICK_EVENT;
                    msg.arg1 = position;
                    mHandler.sendMessage(msg);
                }
            });

            initSelectItem();
            return rootView;
        }

        @Override
        public void onRefresh() {
            mHandler.sendEmptyMessageDelayed(REFRESH_COMPLETE, 0);
        }
    }

    /**
     *
     */
    public static class FunctionFragment extends Fragment {
        static String mLastIp = "";
        static String mCurIp = "";
        TextView curIp;
        TextView lastIp;

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_function, container, false);

            Button btnCutMode = (Button) rootView.findViewById(R.id.btn_airplane_mode);
            Button btnRun = (Button) rootView.findViewById(R.id.btn_run_script_once);
            Button btnStop = (Button) rootView.findViewById(R.id.btn_stop_run);
            Button btnRefresh = (Button) rootView.findViewById(R.id.btn_refresh);
            curIp = (TextView) rootView.findViewById(R.id.tv_cur_ip);
            lastIp = (TextView) rootView.findViewById(R.id.tv_last_ip);

            mCurIp = Cmd.execRootCmd("curl -s whatismyip.akamai.com");
            curIp.setText(mCurIp);
            lastIp.setText(mLastIp);

            btnCutMode.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mLastIp = mCurIp;
                    Cmd.execRootCmd("settings put global airplane_mode_on 1");
                    Cmd.execRootCmd("am broadcast -a android.intent.action.AIRPLANE_MODE --ez state true");
                    SystemClock.sleep(2000);
                    Cmd.execRootCmd("settings put global airplane_mode_on 0");
                    Cmd.execRootCmd("am broadcast -a android.intent.action.AIRPLANE_MODE --ez state false");
                    SystemClock.sleep(10000);

                }
            });

            btnRun.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Runnable runner = new Runnable() {
                        @Override
                        public void run() {
                            File file = new File("/mnt/sdcard/GodHand/tmp");
                            if (!file.exists()) {
                                file.mkdirs();
                            }
//                            FileUtils.writeFile("/mnt/sdcard/GodHand/tmp/run_file", "FacebookOps.lua");
                            Config.getInstance().setProperty("run_file", "FacebookOps.lua");
                            Config.getInstance().save();
                            Cmd.execRootCmd(START_UIAUTOMATOR);
                        }
                    };
                    ThreadPool.add(runner);
                }
            });

            btnStop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Cmd.executeRootCmdNoResult(STOP_UIAUTOMATOR1);
                    Cmd.executeRootCmdNoResult(STOP_UIAUTOMATOR2);
                }
            });

            btnRefresh.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCurIp = Cmd.execRootCmd("curl -s whatismyip.akamai.com");
                    curIp.setText(mCurIp);
                    lastIp.setText(mLastIp);
                }
            });
            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch (position) {
                case 0:
                    return new PlaceholderFragment();
                case 1:
                    return new FunctionFragment();
            }

            return null;
        }

        @Override
        public int getCount() {
            // Show total pages.
            return 1;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";
                case 2:
                    return "SECTION 3";
            }
            return null;
        }
    }

    private class VolumeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if ("android.media.VOLUME_CHANGED_ACTION".equals(intent.getAction())) {

                int type = intent.getIntExtra("android.media.EXTRA_VOLUME_STREAM_TYPE", AudioManager.STREAM_MUSIC);

                //EXTRA_VOLUME_STREAM_VALUE
                //EXTRA_PREV_VOLUME_STREAM_VALUE
                int currVolume = mAudioManager.getStreamVolume(type);
                int maxVolume = mAudioManager.getStreamMaxVolume(type);
                if (mOldVolume < 0) {
                    mOldVolume = intent.getIntExtra("android.media.EXTRA_PREV_VOLUME_STREAM_VALUE", currVolume);
                }

                if ((System.currentTimeMillis() - mTimeout) > 500) {
                    if (currVolume == 0 || currVolume < mOldVolume) {
                        if (mIsAlarmStarted) {
                            toast("请按音量+键关闭正在循环执行的脚本");
                        } else {
                            if (!mIsStarted) {
                                toast("启动脚本");
                                Runnable runner = new Runnable() {
                                    @Override
                                    public void run() {
//                                        Cmd.execRootCmd("echo main.lua > " + luaPath + "/tmp/run_file");
                                        Cmd.executeRootCmdNoResult(START_UIAUTOMATOR);
                                        mIsStarted = false;
                                        toast("脚本执行结束");
                                    }
                                };

                                ThreadPool.add(runner);
                            } else {
                                toast("停止脚本");
                                Runnable runner = new Runnable() {
                                    @Override
                                    public void run() {
                                        Cmd.executeRootCmdNoResult(STOP_UIAUTOMATOR1);
                                        Cmd.executeRootCmdNoResult(STOP_UIAUTOMATOR2);
                                    }
                                };

                                ThreadPool.add(runner);
                            }
                            mIsStarted = !mIsStarted;
                        }
                    }
                }
                mOldVolume = currVolume;
                mTimeout = System.currentTimeMillis();
            }
        }
    }

    private void registerVolumeChangeReceiver() {
        mVolumeReceiver = new VolumeReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.media.VOLUME_CHANGED_ACTION");
        registerReceiver(mVolumeReceiver, filter);
    }

    public void toast(String text) {
        Message msg = msgHandler.obtainMessage();
        msg.arg1 = MSG_TOAST;
        Bundle bundle = new Bundle();
        bundle.putString("text", text);
        msg.setData(bundle);
        msgHandler.sendMessage(msg);
    }
}
