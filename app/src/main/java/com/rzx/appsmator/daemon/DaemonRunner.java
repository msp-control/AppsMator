package com.rzx.appsmator.daemon;

import com.rzx.appsmator.cmd.Cmd;

/**
 * Created by Administrator on 2016/9/1.
 */
public class DaemonRunner {
    private static final String STOP_UIAUTOMATOR1 =
            "am force-stop com.rzx.godhandmator";
    private static final String STOP_UIAUTOMATOR2 =
            "am force-stop com.rzx.godhandmator.test";
    private static final String KILL_AM_INSTRUMENT =
            "busybox ps -ef|busybox grep app_process |busybox grep -v 'grep'|busybox grep instrument|busybox awk '{print $1}'|busybox xargs kill -9";
    private static final String KILL_LOOP_RUN =
            "busybox ps -ef|grep rzx_runner|busybox grep -v 'grep'|busybox awk '{print $1}'|busybox xargs kill -9";
    private static final String START_LOOP_RUN =
            "rzx_runner d";
    private static final String IS_LOOP_RUNNING =
            "busybox ps -ef|grep rzx_runner|busybox grep -v 'grep'|busybox awk '{print $1}'";

    public static Boolean isLoopRunning(){
        String isRunning = Cmd.execRootCmd(IS_LOOP_RUNNING);
        return !isRunning.isEmpty();
    }

    public static void startLoopRun(){
        Cmd.execRootCmd(START_LOOP_RUN);
    }

    public static void stopLoopRun(){
        Cmd.executeRootCmdNoResult(STOP_UIAUTOMATOR1);
        Cmd.executeRootCmdNoResult(STOP_UIAUTOMATOR2);
        Cmd.executeRootCmdNoResult(KILL_AM_INSTRUMENT);
        Cmd.executeRootCmdNoResult(KILL_LOOP_RUN);
        Cmd.executeRootCmdNoResult(STOP_UIAUTOMATOR1);
        Cmd.executeRootCmdNoResult(STOP_UIAUTOMATOR2);
    }
}
