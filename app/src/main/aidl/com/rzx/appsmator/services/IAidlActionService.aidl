// IAidlActionService.aidl
package com.rzx.appsmator.services;

// Declare any non-default types here with import statements
import com.rzx.appsmator.model.Tb_contacts;

interface IAidlActionService {

    void toast(String str);

    String getDeviceId();

    void addContacts(in List<Tb_contacts> contacts);

    void clearContacts();
}
